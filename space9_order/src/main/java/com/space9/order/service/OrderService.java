package com.space9.order.service;

import java.util.List;

import com.space9.common.pojo.Space9Result;
import com.space9.pojo.TbOrder;
import com.space9.pojo.TbOrderItem;
import com.space9.pojo.TbOrderShipping;

public interface OrderService {

	Space9Result createOrder(TbOrder order, List<TbOrderItem> itemList, TbOrderShipping orderShipping);

}
