package com.space9.search.dao;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import com.space9.search.pojo.SearchResult;

public interface SearchDao {

	SearchResult search(SearchRequest searchRequest) throws Exception;
}
