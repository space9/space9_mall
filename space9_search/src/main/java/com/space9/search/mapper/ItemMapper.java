package com.space9.search.mapper;

import java.util.List;

import com.space9.search.pojo.Item;

public interface ItemMapper {

	List<Item> getItemList();
}
