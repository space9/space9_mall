package com.space9.search.service;

import com.space9.common.pojo.Space9Result;

public interface ItemService {

	Space9Result importAllItems();

}
