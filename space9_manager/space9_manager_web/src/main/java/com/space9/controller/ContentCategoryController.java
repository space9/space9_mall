package com.space9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.space9.common.pojo.EUTreeNode;
import com.space9.common.pojo.Space9Result;
import com.space9.service.ContentCategoryService;

@Controller
@RequestMapping("/content/category")
public class ContentCategoryController {

	@Autowired
	private ContentCategoryService contentCategoryService;
	
	@RequestMapping("/list")
	@ResponseBody
	public List<EUTreeNode> getContentCatList(@RequestParam(value="id", defaultValue="0")Long parentId) {
		List<EUTreeNode> list = contentCategoryService.getCategoryList(parentId);
		return list;
	}
	
	@RequestMapping("/create")
	@ResponseBody
	public Space9Result createContentCategory(Long parentId, String name) {
		Space9Result result = contentCategoryService.insertContentCategory(parentId, name);
		return result;
	}
	@RequestMapping("/delete")
	@ResponseBody
	public Space9Result deleteContentCategory(Long parentId, Long id) {
		Space9Result result = contentCategoryService.deleteContentCategory(parentId, id);
		return result;
	}
}
