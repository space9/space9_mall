package com.space9.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.space9.common.pojo.EUDataGridResult;
import com.space9.common.pojo.Space9Result;
import com.space9.pojo.TbContent;
import com.space9.service.ContentService;

@Controller
@RequestMapping("/content")
public class ContentController {

	@Autowired
	private ContentService contentService;
	
	@RequestMapping("/save")
	@ResponseBody
	public Space9Result insertContent(TbContent content) {
		Space9Result result = contentService.insertContent(content);
		return result;
	}
	@RequestMapping("/query/list")
	@ResponseBody
	public EUDataGridResult getContentList(long categoryId, Integer page, Integer rows) {
		EUDataGridResult result = contentService.getContentList(categoryId, page, rows);
		return result;
	}
}
