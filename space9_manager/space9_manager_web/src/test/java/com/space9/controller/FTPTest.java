package com.space9.controller;

import java.io.File;
import java.io.FileInputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.junit.Test;

import com.space9.common.utils.FtpUtil;

public class FTPTest {

	@Test
	public void testFtpClient() throws Exception {
		//创建一个FtpClient对象
		FTPClient ftpClient = new FTPClient();
		//创建FTP连接，默认是21号端口
		ftpClient.connect("192.168.121.132",21);
		//登录FTP服务器，使用用户名和密码
		ftpClient.login("ftpuser", "ftpuser");
		//上传文件
		FileInputStream inputStream = new FileInputStream(new File("D:\\12.jpg"));
		//设置为被动模式
		ftpClient.enterLocalPassiveMode();
		ftpClient.changeWorkingDirectory("/home/ftpuser/www/images");
		ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
		ftpClient.storeFile("test1.jpg", inputStream);
		inputStream.close();
		//关闭连接
		ftpClient.logout();
		ftpClient.disconnect();
	}
	
	@Test
	public void testFtpUtil() throws Exception {
		FileInputStream inputStream = new FileInputStream(new File("D:\\12.jpg"));  
        FtpUtil.uploadFile("192.168.208.128", 21, "ftpuser", "ftpuser", "/home/ftpuser/www/images","/2015/01/21", "test1.jpg", inputStream);
	}
}
