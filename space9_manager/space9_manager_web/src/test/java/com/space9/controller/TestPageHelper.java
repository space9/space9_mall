/**
 * 
 */
package com.space9.controller;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.space9.mapper.TbItemParamMapper;
import com.space9.pojo.TbItemParam;
import com.space9.pojo.TbItemParamExample;

/**
 * @author The Space
 *
 */
public class TestPageHelper {
	
	private ApplicationContext applicationContext;

	@Test
	public void testPageHelper(){
		applicationContext = new ClassPathXmlApplicationContext("classpath:spring/applicationContext_*.xml");
		//从spring容器中获得Mapper的代理对象
		TbItemParamMapper mapper = applicationContext.getBean(TbItemParamMapper.class);
		//执行查询，并分页
		TbItemParamExample example = new TbItemParamExample();
		//分页处理
		PageHelper.startPage(1, 10);
		List<TbItemParam> list = mapper.selectByExampleWithBLOBs(example);
		//获取规格参数列表
		for (TbItemParam tbItemParam : list) {
			System.out.println(tbItemParam.getParamData());
		}
		//获取分页信息
		PageInfo<TbItemParam> pageInfo = new PageInfo<>(list);
		long total = pageInfo.getTotal();
		System.out.println("规格参数总数：" + total);
	}
}
