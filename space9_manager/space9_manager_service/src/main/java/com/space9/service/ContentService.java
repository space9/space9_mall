package com.space9.service;

import com.space9.common.pojo.EUDataGridResult;
import com.space9.common.pojo.Space9Result;
import com.space9.pojo.TbContent;

public interface ContentService {

	Space9Result insertContent(TbContent content);

	EUDataGridResult getContentList(long categoryId, int page,int rows);

}
