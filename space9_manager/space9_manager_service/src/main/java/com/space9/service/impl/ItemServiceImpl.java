package com.space9.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.space9.common.pojo.EUDataGridResult;
import com.space9.common.pojo.Space9Result;
import com.space9.common.utils.IDUtils;
import com.space9.mapper.TbItemDescMapper;
import com.space9.mapper.TbItemMapper;
import com.space9.mapper.TbItemParamItemMapper;
import com.space9.pojo.TbItem;
import com.space9.pojo.TbItemDesc;
import com.space9.pojo.TbItemExample;
import com.space9.pojo.TbItemExample.Criteria;
import com.space9.pojo.TbItemParamItem;
import com.space9.service.ItemService;

@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	private TbItemMapper itemMapper;
	
	@Autowired
	private TbItemDescMapper itemDescMapper;
	
	@Autowired
	private TbItemParamItemMapper itemParamItemMapper;
	
	@Override
	public TbItem getItemById(long itemId) {
		
		TbItemExample example = new TbItemExample();
		Criteria criteria = example.createCriteria();
		criteria.andIdEqualTo(itemId);
		List<TbItem> list = itemMapper.selectByExample(example);
		if (list != null && list.size() > 0) {
			TbItem item = list.get(0);
			return item;
		}
		return null;
	}
	@Override
	public EUDataGridResult getItemList(int page, int rows){
		//查询商品列表
		TbItemExample example = new TbItemExample();
		//分页处理
		PageHelper.startPage(page, rows);
		List<TbItem> list = itemMapper.selectByExample(example);
		//创建一个返回值对象
		EUDataGridResult result = new EUDataGridResult();
		result.setRows(list);
		//获取记录总条数
		PageInfo<TbItem> pageInfo = new PageInfo<>(list);
		result.setTotal(pageInfo.getTotal());
		return result;
	}
	@Override
	public Space9Result createItem(TbItem item, String desc, String itemParam) throws Exception {
		//item补全
		//生成商品ID
		Long itemId = IDUtils.genItemId();
		item.setId(itemId);
		//商品状态：1-正常 2-下架 3-删除
		item.setStatus((byte) 1);
		item.setCreated(new Date());
		item.setUpdated(new Date());
		//插入到数据库
		itemMapper.insert(item);
		//添加商品描述信息
		Space9Result result1 = insertItemDesc(itemId, desc);
		if(result1.getStatus() != 200) {
			throw new Exception();
		}
		//添加规格参数
		Space9Result result2 = insertItemParamItem(itemId, itemParam);
		if(result2.getStatus() != 200) {
			throw new Exception();
		}
		return Space9Result.ok();
	}
	
	private Space9Result insertItemDesc(Long itemId, String desc) {
		TbItemDesc itemDesc = new TbItemDesc();
		itemDesc.setItemId(itemId);
		itemDesc.setItemDesc(desc);
		itemDesc.setCreated(new Date());
		itemDesc.setUpdated(new Date());
		itemDescMapper.insert(itemDesc);
		return Space9Result.ok();
	}
	
	private Space9Result insertItemParamItem(Long itemId, String itemParam) {
		//创建一个pojo
		TbItemParamItem itemParamItem = new TbItemParamItem();
		itemParamItem.setItemId(itemId);
		itemParamItem.setParamData(itemParam);
		itemParamItem.setCreated(new Date());
		itemParamItem.setUpdated(new Date());
		//向表中插入数据
		itemParamItemMapper.insert(itemParamItem);
		return Space9Result.ok();
	}
	
	@Override
	public Space9Result deleteItem(Long ids) throws Exception {
		itemMapper.deleteByPrimaryKey(ids);
		//System.out.print("商品Id:"+ids);
		return Space9Result.ok();
	}

}
