package com.space9.service;

import java.util.List;

import com.space9.common.pojo.EUTreeNode;
import com.space9.common.pojo.Space9Result;

public interface ContentCategoryService {

	List<EUTreeNode> getCategoryList(long parentId);

	Space9Result insertContentCategory(long parentId, String name);

	Space9Result deleteContentCategory(Long parentId, Long id);
}
