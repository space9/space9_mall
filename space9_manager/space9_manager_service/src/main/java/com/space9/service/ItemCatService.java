package com.space9.service;

import java.util.List;

import com.space9.pojo.TbItemCat;

public interface ItemCatService {

	public List<TbItemCat> getItemCatList(long parentId);
}
