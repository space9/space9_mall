package com.space9.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.space9.common.pojo.EUTreeNode;
import com.space9.common.pojo.Space9Result;
import com.space9.mapper.TbContentCategoryMapper;
import com.space9.pojo.TbContentCategory;
import com.space9.pojo.TbContentCategoryExample;
import com.space9.pojo.TbContentCategoryExample.Criteria;
import com.space9.service.ContentCategoryService;

@Service
public class ContentCategoryServiceImpl implements ContentCategoryService {

	@Autowired
	private TbContentCategoryMapper contentCategoryMapper;
	@Override
	public List<EUTreeNode> getCategoryList(long parentId) {
		//根据parentid查询节点列表
		TbContentCategoryExample example = new TbContentCategoryExample();
		Criteria criteria = example.createCriteria();
		criteria.andParentIdEqualTo(parentId);
		//执行查询
		List<TbContentCategory> list = contentCategoryMapper.selectByExample(example);
		List<EUTreeNode> resultList = new ArrayList<>();
		for (TbContentCategory tbContentCategory : list) {
			//创建一个节点
			EUTreeNode node = new EUTreeNode();
			node.setId(tbContentCategory.getId());
			node.setText(tbContentCategory.getName());
			node.setState(tbContentCategory.getIsParent()?"closed":"open");
			
			resultList.add(node);
		}
		return resultList;
	}
	
	@Override
	public Space9Result insertContentCategory(long parentId, String name) {
		
		//创建一个pojo
		TbContentCategory contentCategory = new TbContentCategory();
		contentCategory.setName(name);
		contentCategory.setIsParent(false);
		//'状态。可选值:1(正常),2(删除)',
		contentCategory.setStatus(1);
		contentCategory.setParentId(parentId);
		contentCategory.setSortOrder(1);
		contentCategory.setCreated(new Date());
		contentCategory.setUpdated(new Date());
		//添加记录
		contentCategoryMapper.insert(contentCategory);
		
		//查询主键id并返回TbContentCategory的pojo
		TbContentCategoryExample example = new TbContentCategoryExample();
		Criteria criteria = example.createCriteria();
		criteria.andParentIdEqualTo(parentId);
		contentCategory = (TbContentCategory) contentCategoryMapper.selectByExample(example).get(0);
		
		//查看父节点的isParent列是否为true，如果不是true改成true
		TbContentCategory parentCat = contentCategoryMapper.selectByPrimaryKey(parentId);
		//判断是否为true
		if(!parentCat.getIsParent()) {
			parentCat.setIsParent(true);
			//更新父节点 
			contentCategoryMapper.updateByPrimaryKey(parentCat);
		}  
		//返回结果
		return Space9Result.ok(contentCategory);
	}

	@Override
	public Space9Result deleteContentCategory(Long parentId, Long id) {
		contentCategoryMapper.deleteByPrimaryKey(id);
		
		//查看父节点是否还有子节点
		TbContentCategory parentCat = contentCategoryMapper.selectByPrimaryKey(parentId);
		
		TbContentCategoryExample example = new TbContentCategoryExample();
		Criteria criteria = example.createCriteria();
		criteria.andParentIdEqualTo(parentCat.getId());
		List<TbContentCategory> list = contentCategoryMapper.selectByExample(example);
		if(list.isEmpty()){
			parentCat.setIsParent(false);
			//更新父节点 
			contentCategoryMapper.updateByPrimaryKey(parentCat);
		}
		
		return Space9Result.ok();
	}

}
