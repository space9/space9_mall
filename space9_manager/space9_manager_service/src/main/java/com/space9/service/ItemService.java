package com.space9.service;

import com.space9.common.pojo.EUDataGridResult;
import com.space9.common.pojo.Space9Result;
import com.space9.pojo.TbItem;

public interface ItemService {
	TbItem getItemById(long itemId);
	EUDataGridResult getItemList(int page,int rows);
	Space9Result createItem(TbItem item, String desc, String itemParam) throws Exception;
	Space9Result deleteItem(Long ids) throws Exception;
}
