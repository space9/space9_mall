package com.space9.service;

public interface ItemParamItemService {

	String getItemParamByItemId(Long itemId);
}
