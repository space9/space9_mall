package com.space9.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.space9.common.pojo.EUDataGridResult;
import com.space9.common.pojo.Space9Result;
import com.space9.mapper.TbItemParamMapper;
import com.space9.pojo.TbItem;
import com.space9.pojo.TbItemExample;
import com.space9.pojo.TbItemParam;
import com.space9.pojo.TbItemParamExample;
import com.space9.pojo.TbItemParamExample.Criteria;
import com.space9.service.ItemParamService;

@Service
public class ItemParamServiceImpl implements ItemParamService {

	@Autowired
	private TbItemParamMapper itemParamMapper;
	
	@Override
	public Space9Result getItemParamByCid(long cid) {
		TbItemParamExample example = new TbItemParamExample();
		Criteria criteria = example.createCriteria();
		criteria.andItemCatIdEqualTo(cid);
		List<TbItemParam> list = itemParamMapper.selectByExampleWithBLOBs(example);
		//判断是否查询到结果
		if (list != null && list.size() > 0) {
			return Space9Result.ok(list.get(0));
		}
		
		return Space9Result.ok();
	}
	
	@Override
	public EUDataGridResult getItemParamList(int page, int rows){
		//查询商品规格信息列表
		TbItemParamExample example = new TbItemParamExample();
		//分页处理
		PageHelper.startPage(page, rows);
		List<TbItemParam> list = itemParamMapper.selectByExampleWithBLOBs(example);
		//创建一个返回值对象
		EUDataGridResult result = new EUDataGridResult();
		result.setRows(list);
		//获取记录总条数
		PageInfo<TbItemParam> pageInfo = new PageInfo<>(list);
		result.setTotal(pageInfo.getTotal());
		return result;
	}

	@Override
	public Space9Result insertItemParam(TbItemParam itemParam) {
		//补全pojo
		itemParam.setCreated(new Date());
		itemParam.setUpdated(new Date());
		//插入到规格参数模板表
		itemParamMapper.insert(itemParam);
		return Space9Result.ok();
	}

}
