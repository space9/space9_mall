package com.space9.service;

import com.space9.common.pojo.EUDataGridResult;
import com.space9.common.pojo.Space9Result;
import com.space9.pojo.TbItemParam;

public interface ItemParamService {

	Space9Result getItemParamByCid(long cid);
	EUDataGridResult getItemParamList(int page,int rows);
	Space9Result insertItemParam(TbItemParam itemParam);
}
