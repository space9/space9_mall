package com.space9.sso.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.space9.common.pojo.Space9Result;
import com.space9.pojo.TbUser;

public interface UserService {

	Space9Result checkData(String content, Integer type);

	Space9Result createUser(TbUser user);

	Space9Result getUserByToken(String token);

	Space9Result userLogin(String username, String password, HttpServletRequest request, HttpServletResponse response);

	Space9Result logout(String token, HttpServletRequest request, HttpServletResponse response);
	
}
