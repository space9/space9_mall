package com.space9.sso.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.space9.common.pojo.Space9Result;
import com.space9.common.utils.JsonUtils;
import com.space9.pojo.TbUser;
import com.space9.sso.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/check/{param}/{type}", 
	produces=MediaType.APPLICATION_JSON_VALUE + ";charset=utf-8")
	@ResponseBody
	public Object checkData(@PathVariable String param, @PathVariable Integer type, String callback) {
		Space9Result result = null;
		
		//参数有效性校验
		if (type != 1 && type != 2 && type != 3 ) {
			result = Space9Result.build(400, "校验内容类型错误");
		}
		//校验出错
		if (null != result) {
			if (null != callback) {
				String json = JsonUtils.objectToJson(result);
				return callback + "(" + json + ");";
			} else {
				return result; 
			}
		}
		//调用服务
		try {
			result = userService.checkData(param, type);
			
		} catch (Exception e) {
			result = Space9Result.build(500, e.getMessage());
		}
		
		if (null != callback) {
			String json = JsonUtils.objectToJson(result);
			return callback + "(" + json + ");";
		} else {
			return result; 
		}
	}
	
	//创建用户
	@RequestMapping(value="/register", method=RequestMethod.POST)
	@ResponseBody
	public Space9Result createUser(TbUser user) {
		
		try {
			Space9Result result = userService.createUser(user);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return Space9Result.build(500, e.getMessage());
		}
	}
	
	//用户登录
	@RequestMapping(value="/login", method=RequestMethod.POST)
	@ResponseBody
	public Space9Result userLogin(String username, String password, HttpServletRequest request, HttpServletResponse response) {
		try {
			Space9Result result = userService.userLogin(username, password, request, response);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return Space9Result.build(500, e.getMessage());
		}
	}
	
	//用户注销
	@RequestMapping("/logout/{token}")
	@ResponseBody
	public Space9Result logout(@PathVariable String token, HttpServletRequest request, HttpServletResponse response){
		Space9Result result = userService.logout(token, request, response);
		return result;
	}
	
	@RequestMapping("/token/{token}")
	@ResponseBody
	public Object getUserByToken(@PathVariable String token, String callback) {
		Space9Result result = null;
		try {
			result = userService.getUserByToken(token);
		} catch (Exception e) {
			e.printStackTrace();
			result = Space9Result.build(500, e.getMessage());
		}
		
		//判断是否为jsonp调用
		if (StringUtils.isBlank(callback)) {
			return result;
		} else {
			String json = JsonUtils.objectToJson(result);
			return callback + "(" + json + ");";
		}
		
	}
}
