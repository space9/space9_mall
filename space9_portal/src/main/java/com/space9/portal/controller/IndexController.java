package com.space9.portal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.space9.portal.service.ContentService;

@Controller
public class IndexController {

	@Autowired
	private ContentService contentService;
	
	@RequestMapping("/index")
	public String showIndex(Model model) {
		String adJson = contentService.getContentList();
		String adJson2 = contentService.getContentList2();
		String adJson3 = contentService.getContentList3();
		String adJson4 = contentService.getContentList4();
		model.addAttribute("ad1", adJson);
		model.addAttribute("ad2", adJson2);
		model.addAttribute("ad3", adJson3);
		model.addAttribute("ad4", adJson4);
		return "index";
	}
	
	@RequestMapping(value="/httpclient/post", method=RequestMethod.POST, produces=MediaType.TEXT_PLAIN_VALUE+";charset=utf-8")
	@ResponseBody
	public String testPost(String username, String password) {
		return "username:"+username+"\tpassword:"+password;
	}
	
}
