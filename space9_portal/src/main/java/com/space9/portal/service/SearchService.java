package com.space9.portal.service;

import com.space9.portal.pojo.SearchResult;

public interface SearchService {

	SearchResult search(String queryString, int page);

}
