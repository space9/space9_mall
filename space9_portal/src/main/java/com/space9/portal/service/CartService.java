package com.space9.portal.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.space9.common.pojo.Space9Result;
import com.space9.portal.pojo.CartItem;

public interface CartService {

	Space9Result deleteCartItem(long itemId, HttpServletRequest request, HttpServletResponse response);

	List<CartItem> getCartItemList(HttpServletRequest request, HttpServletResponse response);

	Space9Result addCartItem(long itemId, int num, HttpServletRequest request, HttpServletResponse response);

}
