package com.space9.portal.service;

import com.space9.pojo.TbUser;

public interface UserService {

	TbUser getUserByToken(String token);

}
