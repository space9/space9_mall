package com.space9.portal.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.space9.common.pojo.Space9Result;
import com.space9.common.utils.HttpClientUtil;
import com.space9.common.utils.JsonUtils;
import com.space9.portal.pojo.Order;
import com.space9.portal.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {
	
	@Value("${ORDER_BASE_URL}")
	private String ORDER_BASE_URL;
	@Value("${ORDER_CREATE_URL}")
	private String ORDER_CREATE_URL;
	

	@Override
	public String createOrder(Order order) {
		//调用创建订单服务之前补全用户信息。
		//从cookie中后取TT_TOKEN的内容，根据token调用sso系统的服务根据token换取用户信息。
		
		//调用space9_order的服务提交订单。
		String json = HttpClientUtil.doPostJson(ORDER_BASE_URL + ORDER_CREATE_URL, JsonUtils.objectToJson(order));
		System.out.println("创建订单OK"+json);
		//把json转换成space9Result
		Space9Result space9Result = Space9Result.format(json);
		if (space9Result.getStatus() == 200) {
			Object orderId = space9Result.getData();
			return orderId.toString();
		}
		return "";
	}

}