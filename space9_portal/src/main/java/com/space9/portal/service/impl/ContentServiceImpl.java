package com.space9.portal.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.space9.common.pojo.Space9Result;
import com.space9.common.utils.HttpClientUtil;
import com.space9.common.utils.JsonUtils;
import com.space9.pojo.TbContent;
import com.space9.portal.service.ContentService;

@Service
public class ContentServiceImpl implements ContentService {

	@Value("${REST_BASE_URL}")
	private String REST_BASE_URL;
	@Value("${REST_INDEX_AD_URL}")
	private String REST_INDEX_AD_URL;
	@Value("${REST_INDEX_AD_URL2}")
	private String REST_INDEX_AD_URL2;
	@Value("${REST_INDEX_AD_URL3}")
	private String REST_INDEX_AD_URL3;
	@Value("${REST_INDEX_AD_URL4}")
	private String REST_INDEX_AD_URL4;
	
	@Override
	public String getContentList() {
		//调用服务层的服务
		String result = HttpClientUtil.doGet(REST_BASE_URL + REST_INDEX_AD_URL);
		//把字符串转换成space9Reuslt
		try {
			Space9Result space9Reuslt = Space9Result.formatToList(result, TbContent.class);
			//取内容列表
			List<TbContent> list = (List<TbContent>) space9Reuslt.getData();
			List<Map> resultList = new ArrayList<>();
 			//创建一个jsp页面要求的pojo列表
			for (TbContent tbContent : list) {
				Map map = new HashMap<>();
				map.put("src", tbContent.getPic());
				map.put("height", 240);
				map.put("width", 670);
				map.put("srcB", tbContent.getPic2());
				map.put("widthB", 550);
				map.put("heightB", 240);
				map.put("href", tbContent.getUrl());
				map.put("alt", tbContent.getSubTitle());
				resultList.add(map);
			}
			return JsonUtils.objectToJson(resultList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	@Override
	public String getContentList2() {
		//调用服务层的服务
		String result = HttpClientUtil.doGet(REST_BASE_URL + REST_INDEX_AD_URL2);
		//把字符串转换成space9Reuslt
		try {
			Space9Result space9Reuslt = Space9Result.formatToList(result, TbContent.class);
			//取内容列表
			List<TbContent> list = (List<TbContent>) space9Reuslt.getData();
			List<Map> resultList = new ArrayList<>();
			//创建一个jsp页面要求的pojo列表
			for (TbContent tbContent : list) {
				Map map = new HashMap<>();
				map.put("src", tbContent.getPic());
				map.put("height", 159);
				map.put("width", 202);
				map.put("srcB", tbContent.getPic2());
				map.put("widthB", 202);
				map.put("heightB", 159);
				map.put("href", tbContent.getUrl());
				map.put("alt", tbContent.getSubTitle());
				resultList.add(map);
			}
			return JsonUtils.objectToJson(resultList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	@Override
	public String getContentList3() {
		//调用服务层的服务
		String result = HttpClientUtil.doGet(REST_BASE_URL + REST_INDEX_AD_URL3);
		//把字符串转换成space9Reuslt
		try {
			Space9Result space9Reuslt = Space9Result.formatToList(result, TbContent.class);
			//取内容列表
			List<TbContent> list = (List<TbContent>) space9Reuslt.getData();
			List<Map> resultList = new ArrayList<>();
			//创建一个jsp页面要求的pojo列表
			for (TbContent tbContent : list) {
				Map map = new HashMap<>();
				map.put("src", tbContent.getPic());
				map.put("height", 70);
				map.put("width", 310);
				map.put("srcB", tbContent.getPic2());
				map.put("widthB", 310);
				map.put("heightB", 70);
				map.put("href", tbContent.getUrl());
				map.put("alt", tbContent.getSubTitle());
				resultList.add(map);
			}
			return JsonUtils.objectToJson(resultList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	@Override
	public String getContentList4() {
		//调用服务层的服务
		String result = HttpClientUtil.doGet(REST_BASE_URL + REST_INDEX_AD_URL4);
		//把字符串转换成space9Reuslt
		try {
			Space9Result space9Reuslt = Space9Result.formatToList(result, TbContent.class);
			//取内容列表
			List<TbContent> list = (List<TbContent>) space9Reuslt.getData();
			List<Map> resultList = new ArrayList<>();
			//创建一个jsp页面要求的pojo列表
			for (TbContent tbContent : list) {
				Map map = new HashMap<>();
				map.put("src", tbContent.getPic());
				map.put("height", 180);
				map.put("width", 209);
				map.put("srcB", tbContent.getPic2());
				map.put("widthB", 209);
				map.put("heightB", 180);
				map.put("href", tbContent.getUrl());
				map.put("alt", tbContent.getSubTitle());
				resultList.add(map);
			}
			return JsonUtils.objectToJson(resultList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
