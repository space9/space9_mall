package com.space9.portal.service;

import com.space9.portal.pojo.Order;

public interface OrderService {

	String createOrder(Order order);

}
