package com.space9.portal.service;

import com.space9.portal.pojo.ItemInfo;

public interface ItemService {

	ItemInfo getItemById(Long itemId);

	String getItemDescById(Long itemId);

	String getItemParam(Long itemId);

}
