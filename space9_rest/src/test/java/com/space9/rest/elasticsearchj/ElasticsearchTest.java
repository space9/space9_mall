package com.space9.rest.elasticsearchj;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.junit.Test;

public class ElasticsearchTest {

	@Test
	public void addDocument() throws Exception {
		RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(
                new HttpHost("localhost", 9200, "http")));
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		searchSourceBuilder.from(0); 
		searchSourceBuilder.size(5);
		//searchSourceBuilder.fetchSource(new String[]{"item_category_name"}, new String[]{});
		MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("item_title", "电脑");
		//BoolQueryBuilder boolBuilder = QueryBuilders.boolQuery();
        //boolBuilder.must(matchQueryBuilder);
		searchSourceBuilder.query(matchQueryBuilder); 
		//searchSourceBuilder.query(QueryBuilders.termQuery("analyzer","smartcn"));
		
		
		
		
		
		HighlightBuilder highlightBuilder = new HighlightBuilder(); 
		HighlightBuilder.Field highlightTitle = new HighlightBuilder.Field("item_title"); 
		highlightTitle.highlighterType("unified");  
		highlightBuilder.field(highlightTitle); 
		searchSourceBuilder.highlighter(highlightBuilder);
		
		
		
		SearchRequest searchRequest = new SearchRequest("mydb");
		searchRequest.types("mytable");
		searchRequest.source(searchSourceBuilder);
		SearchResponse searchResponse = client.search(searchRequest);
		
		
		
		
		
		System.out.println("总条数: "+searchResponse.getHits().getTotalHits());
		for(SearchHit hit : searchResponse.getHits())
		{
			Map<String, HighlightField> highlightFields = hit.getHighlightFields();
		    HighlightField highlight = highlightFields.get("item_title"); 
		    Text[] fragments = highlight.fragments();  
		    String fragmentString = fragments[0].string();
		    System.out.println(fragmentString);
		    
			
			
			
		    System.out.println(hit.getSourceAsString());
		    System.out.println(hit.getSourceAsMap().get("id"));
		}
		client.close();
	}
}
