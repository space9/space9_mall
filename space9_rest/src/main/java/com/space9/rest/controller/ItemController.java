package com.space9.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.space9.common.pojo.Space9Result;
import com.space9.rest.service.ItemService;

@Controller
@RequestMapping("/item")
public class ItemController {

	@Autowired
	private ItemService itemService;
	
	@RequestMapping("/info/{itemId}")
	@ResponseBody
	public Space9Result getItemBaseInfo(@PathVariable Long itemId) {
		Space9Result result = itemService.getItemBaseInfo(itemId);
		return result;
	}
	
	@RequestMapping("/desc/{itemId}")
	@ResponseBody
	public Space9Result getItemDesc(@PathVariable Long itemId) {
		Space9Result result = itemService.getItemDesc(itemId);
		return result;
	}
	
	@RequestMapping("/param/{itemId}")
	@ResponseBody
	public Space9Result getItemParam(@PathVariable Long itemId) {
		Space9Result result = itemService.getItemParam(itemId);
		return result;
	}
}