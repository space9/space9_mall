package com.space9.rest.service;

import com.space9.common.pojo.Space9Result;

public interface RedisService {

	Space9Result syncContent(long contentCid);

}
