package com.space9.rest.service;

import com.space9.rest.pojo.CatResult;

public interface ItemCatService {

	CatResult getItemCatList();
}
