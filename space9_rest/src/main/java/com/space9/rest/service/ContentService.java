package com.space9.rest.service;

import java.util.List;

import com.space9.pojo.TbContent;

public interface ContentService {

	List<TbContent> getContentList(long contentCid);

}
