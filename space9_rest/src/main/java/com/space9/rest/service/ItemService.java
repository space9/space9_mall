package com.space9.rest.service;

import com.space9.common.pojo.Space9Result;

public interface ItemService {

	Space9Result getItemBaseInfo(long itemId);

	Space9Result getItemDesc(long itemId);

	Space9Result getItemParam(long itemId);

}
