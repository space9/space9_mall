package com.space9.rest.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.space9.common.pojo.Space9Result;
import com.space9.mapper.TbItemDescMapper;
import com.space9.mapper.TbItemMapper;
import com.space9.mapper.TbItemParamItemMapper;
import com.space9.pojo.TbItem;
import com.space9.pojo.TbItemDesc;
import com.space9.pojo.TbItemParamItem;
import com.space9.pojo.TbItemParamItemExample;
import com.space9.pojo.TbItemParamItemExample.Criteria;
import com.space9.rest.service.ItemService;

@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	private TbItemMapper itemMapper;
	@Autowired
	private TbItemDescMapper itemDescMapper;
	@Autowired
	private TbItemParamItemMapper itemParamItemMapper;
	
	@Override
	public Space9Result getItemBaseInfo(long itemId) {
		//根据商品id查询商品信息
		TbItem item = itemMapper.selectByPrimaryKey(itemId);
		//使用Space9Result包装一下
		
		return Space9Result.ok(item);
	}
	
	@Override
	public Space9Result getItemDesc(long itemId) {
		
		//创建查询条件
		TbItemDesc itemDesc = itemDescMapper.selectByPrimaryKey(itemId);
		
		return Space9Result.ok(itemDesc);
	}
	
	@Override
	public Space9Result getItemParam(long itemId) {
		//根据商品id查询规格参数
		//设置查询条件
		TbItemParamItemExample example = new TbItemParamItemExample();
		Criteria criteria = example.createCriteria();
		criteria.andItemIdEqualTo(itemId);
		//执行查询
		List<TbItemParamItem> list = itemParamItemMapper.selectByExampleWithBLOBs(example);
		if (list != null && list.size()>0) {
			TbItemParamItem paramItem = list.get(0);
			return Space9Result.ok(paramItem);
		}
		return Space9Result.build(400, "无此商品规格");
	}

}